// This script is used in http://okfn.org/local/ to create the list of local groups

jQuery(function($) {
  var url = 'https://docs.google.com/a/okfn.org/spreadsheet/ccc?key=0AqR8dXc6Ji4JdE1YVlozbU9XUzhOcEdidXNHYTdGZ1E#gid=0';
  recline.Backend.GDocs.fetch({url: url})
    .done(function(data) {
      var ftData = $('#ft-data');
      _.each(data.records, function(record) {
        record.href = record.website || record.discussion;
        record.iso = record.iso.toLowerCase();
        record.statusLower = record.status.toLowerCase();
        var out = infoDivTemplate(record);
        ftData.append(out);
      });
    });
});

var infoDivTemplate = _.template(' \
  <li class="<%= statusLower %> active<%= active %>"> \
    <a href="<%= href %>"> \
      <span class="flag <%= iso %>"></span> \
      <span class="text"><%= country %> \
        <span class="status"><%= status %></span> \
      </span> \
    </a> \
');
