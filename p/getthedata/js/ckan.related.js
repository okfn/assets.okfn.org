$(document).ready(function($) {
    
  var query = []; //"(" + $(".headNormal h1 a").text() + ")"; 
  $(".post-tag").each(function(i, e) {query.push($(e).text());})
  query = query.join(' OR '); 

  insertResults = function(reply) {
    var elem = "<div style='background-color: #F5D0A9;' class='boxC'><p><img src='http://assets.okfn.org/p/ckan/img/ckan_logo_small_symbol.png' width='20' height='20'> <strong>Related datasets</strong></p><ul>";
    reply.results.forEach(function(result) {  
        elem += "<li><a href='" + result.ckan_url + "'><strong>" + result.title + "</strong></a></li>"; 
    }); 
    elem += "</ul><a href='http://ckan.net/package?q=" + escape(query) + "'>Find more on CKAN...</a></div>"; 
    //elem += "<li><a href='http://ckan.net/package?q=" + escape(query) + "'>Find more...</a></li></ul></div>"; 
    $("#subscription_box").after(elem); 
  };

  insertResultsDataGovUk = function(reply) {
    var elem = "<div style='background-color: #F5D0A9;' class='boxC'><p><strong>Related datasets (Data.Gov.UK)</strong></p><ul>";
    reply.results.forEach(function(result) {  
        elem += "<li><a href='" + result.ckan_url + "'><strong>" + result.title + "</strong></a></li>"; 
    }); 
    elem += "</ul><a href='http://data.gov.uk/datasets'>Find more on Data.Gov.UK ...</a></div>"; 
    //elem += "<li><a href='http://ckan.net/package?q=" + escape(query) + "'>Find more...</a></li></ul></div>"; 
    $("#subscription_box").after(elem); 
  };
  
  if (query.length > 1) {
    $.ajax({
      url: "http://ckan.net/api/search/package?q=" + escape(query) + "&all_fields=1&limit=5", 
      dataType: "jsonp", 
      success: insertResults
    });
    // TODO: re-enable once data.gov.uk endpoint supports OR queries
    // var queryUrl = "http://catalogue.data.gov.uk/api/search/package?q=" + escape(query) + "&all_fields=1&limit=5";
    // $.ajax({
    //  url: queryUrl,
    //  dataType: "jsonp",
    //  success: insertResultsDataGovUk
    // });
  }
}); 

