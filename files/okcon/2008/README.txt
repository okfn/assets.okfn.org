Slides
======

http://www.archive.org/details/okcon_2008

intro_rufus_pollock/
DavePuplett-VIF.pdf
LisaPetrides-OER.pdf
GaelVaroquaux-Mayavi.pdf
MartinAlbrecht-SAGE.pdf
MukiHaklay-EnvironmentalInformation.pdf
JoergBaach-Opencoin.pdf

Pictures
========

http://www.archive.org/details/okcon_2008

Audio
=====

http://www.archive.org/details/okcon_2008

Video
=====

For video see: http://www.archive.org/details/OkCon2008

