var OKFN = OKFN || {};

OKFN.Talks = {
  embed: function(url, data) {
    var text = '';
    text += '<h4 class="slides-link"><a href="' + url + '">' + data.title + '</a></h4>';
    text += '<p>' + data.location + ' ' + data.date + '</p>';
    text += '<iframe src="' + url + '/index.html" class="slides"></iframe>';
    return text;
  }
}
