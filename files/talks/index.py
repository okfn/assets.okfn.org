import os
import re
import json

exclude = ['s5', 'media']

def ourfilter(filenames):
    out = filter(os.path.isdir, filenames)
    out = [ x for x in out if x not in exclude ]
    return out

metare = r'<meta name="([^"]+)" content="([^"]+)" ?/>'
titlere = r'<title>(.+)</title>'

def find_metadata(directory):
    filename = os.path.join(directory, 'index.html')
    if not os.path.exists(filename):
        return None
    fo = open(filename)
    content = fo.read()
    fo.close()
    res = {
        'title': '',
        'date': '',
        'location': '',
        'event': ''
    }
    foundmeta = re.findall(metare, content)
    title = re.findall(titlere, content, re.MULTILINE | re.DOTALL)
    if title:
        title = title[0].strip()
    else:
        title = ''
    for found in foundmeta:
        # ignore meta we don't want e.g. controlVis
        if found[0] in ['date', 'location', 'event']:
            res[found[0]] = found[1]
    # no metadata found in meta tags
    if not res['date']:
        ourdate = re.findall(r'(\d\d\d\d-\d\d-\d\d)', title)
        if ourdate:
            res['date'] = ourdate[0]
        else:
            # finally try directory name
            otherdate = re.findall(r'(\d\d\d\d\d\d\d\d)', directory)
            if otherdate:
                otherdate = otherdate[0]
                res['date'] = otherdate[:4] + '-' + otherdate[4:6] + '-' + otherdate[6:]
    res['title'] = title
    return res

def extract():
    res = {}
    for fn in ourfilter(sorted(os.listdir('.'))):
        res[fn] = find_metadata(fn)
    jsonfn = 'index.json'
    jsfn = 'index.js'
    # fo = open(jsonfn, 'w')
    # json.dump(res, fo, indent=2, sort_keys=True)
    # fo.close()
    out = json.dumps(res, indent=2, sort_keys=True)
    out = 'var OKFN_TALKS_LIST = %s;' % out
    fo = open(jsfn, 'w')
    fo.write(out)
    fo.close()

if __name__ == '__main__':
    extract()

