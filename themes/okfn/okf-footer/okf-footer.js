(function($) {
  if (typeof footerlang === 'undefined') {
      footerlang = "en";
  }
  
  $.getJSON('//assets.okfn.org/themes/okfn/okf-footer/okf-footer.json', function(data) {
        var output="<div class='container'><ul>";
        for (var i in data[footerlang].links) {
            output+="<li><a href='" + data[footerlang].links[i].url + "'>" + data[footerlang].links[i].text + "</a></li>";
        }
  
        output+="</ul></div>";
        document.getElementById("okf-footer").innerHTML=output;
  });
})(jQuery);
