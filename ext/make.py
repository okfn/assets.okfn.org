#!/usr/bin/env python
'''%prog {action}

actions:
    thejit
'''
import urllib
import os
import shutil
import types
import optparse
import sys

TMP = 'tmp'
if not os.path.exists(TMP):
    os.makedirs(TMP)

def thejit():
    urls = [
        'http://thejit.org/downloads/Jit-1.1.2.zip',
        'http://thejit.org/downloads/Jit-1.1.3.zip'
        ]
    url = urls[-1]
    fp = url.split('/')[-1]
    print 'Retrieving: %s' % url
    urllib.urlretrieve(url, fp)
    cmd = 'unzip -d %s %s' % (TMP, fp)
    print cmd
    os.system(cmd)
    dest = url.split('/')[-1][:-4]
    outpath = os.path.join(TMP, 'Jit')
    print 'Moving from %s to %s' % (outpath, dest)
    shutil.move(outpath, dest)

def jquery_tablesorter():
    url = 'http://tablesorter.com/jquery.tablesorter.min.js'
    urllib.urlretrieve(url, url.split('/')[-1])


def _extract(methods):
    local_methods = dict(methods)
    for k,v in local_methods.items():
        if not isinstance(v, types.FunctionType) or k.startswith('_'):
            del local_methods[k]
    return local_methods

if __name__ == '__main__':
    _methods = _extract(locals())

    usage = '''%prog {action}

    '''
    usage += '\n    '.join(
        [ '%s: %s' % (name, m.__doc__.split('\n')[0] if m.__doc__ else '') for (name,m)
        in _methods.items() ])
    parser = optparse.OptionParser(usage)
    options, args = parser.parse_args()

    if not args or not args[0] in _methods:
        parser.print_help()
        sys.exit(1)

    method = args[0]
    _methods[method]()

