Partially modded versions of simile timeline and simileajax.

To deploy::

  hg clone https://bitbucket.org/okfn/timeline
  hg clone https://bitbucket.org/okfn/simileajax

  s3cmd sync --acl-public timeline/*bundle* s3://assets.okfn.org/ext/simile/timeline/
  # s3cmd sync --acl-public timeline/timeline-bundle.js s3://assets.okfn.org/ext/simile/timeline/
  # s3cmd sync --acl-public timeline/timeline-bundle.css s3://assets.okfn.org/ext/simile/timeline/
  s3cmd sync --acl-public timeline/images s3://assets.okfn.org/ext/simile/timeline/

  s3cmd sync --acl-public simileajax/images s3://assets.okfn.org/ext/simile/simileajax
  s3cmd sync --acl-public simileajax/*bundle* s3://assets.okfn.org/ext/simile/simileajax/

