#!/bin/sh

set -eu

if ! which aws >/dev/null 2>&1; then
  cat >&2 <<EOM
Hello, this script has been modified to use the official AWS
command line tools. Please run the following to ensure you have
these and have installed them correctly:

    pip install awscli
    aws configure

You should set the default region to be 'us-east-1'. Once you've
done this, you can run

    ./sync.sh --dryrun

to see what will happen when you perform a real sync with

    ./sync.sh

Happy syncing!
EOM
  exit 1
fi

exec aws s3 sync \
  --acl public-read \
  --exclude sync.sh \
  --exclude '.gitignore' \
  --exclude '.git/*' \
  --exclude '.DS_Store' \
  --exclude '*/.DS_Store' \
  --delete \
  "$@" \
  . \
  s3://assets.okfn.org
